# The application is responsible for sending tasks to an exchange.
## Starting the application
* ensure RabbitMQ is running
* mvn spring-boot:run

## Sending Messages

Need to call the following APIs to send messages

* http://localhost:8080/amqp/send/{n}/{r} *where "n" is the number of tasks to be sent to the exchange and "r" is the routing key*
* The routing key is a name with which the receiving queue associates itself in order to receive only those messages which are sent to the exchange alongwith the same routing key

The name of the exchange by default is 'DIRECT_EXCHANGE' but you can change the name to anything by either

* providing the name as command line arguement e.g. mvn spring-boot:run -Dexchange.name=NAME-OF-YOUR-CHOICE or 
* changing the name of exchange in application.properties

But if you are changing the name of exchange here, you also need to modify the exchange name in the message receiver application


package springboot.amqp.sender;

import springboot.amqp.dto.Task;

/**
 * Created by soumya on 7/2/17.
 */
public interface MessageSender {

    Task sendMessageToExchange(String routingKey, Task task);
}

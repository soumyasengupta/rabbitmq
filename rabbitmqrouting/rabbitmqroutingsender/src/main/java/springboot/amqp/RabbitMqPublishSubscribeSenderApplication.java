package springboot.amqp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by soumya on 11/2/17.
 */
@SpringBootApplication
public class RabbitMqPublishSubscribeSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMqPublishSubscribeSenderApplication.class);
    }

}

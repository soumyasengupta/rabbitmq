package springboot.amqp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.amqp.dto.Task;
import springboot.amqp.sender.MessageSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by soumya on 7/2/17.
 */
@RestController
public class MessageSenderController {

    @Autowired
    MessageSender messageSender;

    @RequestMapping(value="/amqp/send/{noOfTimes}/{routingKey}")
    public List<Task> sendMessage(@PathVariable(name = "noOfTimes") Integer noOfTimes,
                                              @PathVariable(name = "routingKey") String routingKey){

            List<Task> taskList = new ArrayList<>();

            for(int count=1;count<=noOfTimes;count++) {
                Task task = new Task(count, "Name_"+count+"_"+routingKey, "Description_"+count+"_"+routingKey);
                task = messageSender.sendMessageToExchange(routingKey, task);
                taskList.add(task);
            }

            return taskList;

    }

}

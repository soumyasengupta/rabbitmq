package springboot.amqp.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import springboot.amqp.config.RabbitConfiguration;
import springboot.amqp.dto.Task;

/**
 * Created by soumya on 7/2/17.
 */
@Component
public class MessageReceiver {

    private static final Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

    @RabbitListener(queues = "${queue.name}")
    public void receiveTask(Task task) {
        logger.debug("Starting Received Task <" + task + ">");
        task.execute();
        logger.debug("Ending Received Task <" + task + ">");
    }

}
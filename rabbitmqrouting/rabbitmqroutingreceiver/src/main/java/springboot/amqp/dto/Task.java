package springboot.amqp.dto;

/**
 * Created by soumya on 8/2/17.
 */
public class Task {

    private Integer count;
    private String name;
    private String description;

    public Task() {
    }

    public Task(Integer count, String name, String description) {
        this.count = count;
        this.name = name;
        this.description = description;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void execute(){
        try {
            Thread.sleep(2000);
        }catch (InterruptedException ie){
            ie.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Task{" +
                "count=" + count +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

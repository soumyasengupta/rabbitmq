# This application is responsible for receiving messages from queues

## Description
* The sender application sends tasks to an exchange alongwith a particular routing key.
* There are multiple queues bound to the exchange each having their own set of routing keys.
* The tasks are received by the the queue bound to the exchange and whose routing key matches with the same coming from the sender.
* The task is a simple POJO that represents a work to be executed by the receiver. It has a method named "execute" containing a "sleep(5000)" to impersonate a job of duration 5 seconds
* The name of the exchange by default is 'DIRECT_EXCHANGE' but you can change the name to anything by either providing the name as command line arguement e.g. mvn spring-boot:run -Dexchange.name=NAME-OF-YOUR-CHOICE or changing the name of exchange in application.properties

## Start multiple receivers
* mvn spring-boot:run -Dserver.port=8081 -Dqueue.name=ABC -Drouting.keys=ALPHA,BRAVO,CHARLIE
* mvn spring-boot:run -Dserver.port=8082 -Dqueue.name=DEF -Drouting.keys=DELTA,ECHO,FOXTROT
* ...
* mvn spring-boot:run -Dserver.port=8090 -Dqueue.name=XYZ -Drouting.keys=XRAY,YANKEE,ZULU

## Testing the application
Now start sending messages to the queue

* http://localhost:8080/amqp/send/{n}/{r} *where "n" is the number of tasks to be sent to the queue and "r" is any one of the routing key names as mentioned before*


You will find the receiving messages appearing in the console of any on of the receivers based on the routing key used

* Starting Received Task <Task{count=5, name='Name\_1\_BLUE', description='Description\_5\_BLUE'}>
* Ending Received Task <Task{count=5, name='Name\_1\_BLUE', description='Description\_5\_BLUE'}>



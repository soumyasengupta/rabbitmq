RabbitMQ Applications using Spring Boot
======================================

Shows how to utilize RabbitMQ
----------------------------------------------------------------

* for a simple sender and receiver application
* for a simple task to be send by a publisher and recieved by a consumer
* for multiple task to be send by a publisher and received by multiple subscribers
* for multiple tasks to be send by a publisher and received by specific subscribers based on routing keys


Requirements
------------

* [Java Platform (JDK) 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Apache Maven 3.x](http://maven.apache.org/)
* [Ensure RabbitMQ is up and running](https://www.rabbitmq.com/)


Running the application
-----------------------

Go through the README files of each individual application to find out how to run each of them

References
----------

* https://techietweak.wordpress.com/2015/08/14/rabbitmq-a-cloud-based-message-oriented-middleware/
* https://www.rabbitmq.com/getstarted.html



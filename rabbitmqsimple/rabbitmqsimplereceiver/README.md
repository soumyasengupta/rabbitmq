# This application is responsible for receiving messages from queues

## Description
* The sender application sends messages to two different queues "BLACK\_QUEUE" and "BLUE\_QUEUE".
* So we will be starting two different receiver applications for listening to each of the two queues

## Start the receivers
* mvn spring-boot:run -Dserver.port=8081 -Dqueue.name=BLACK_QUEUE
* mvn spring-boot:run -Dserver.port=8082 -Dqueue.name=BLUE_QUEUE

## Testing the application
Now start sending messages to both queues using the sender application

* http://localhost:8080/amqp/send/black/{message} *this will send messages to BLACK_QUEUE*
* http://localhost:8080/amqp/send/blue/{message} *this will send messages to BLUE_QUEUE*

You will find the receiving messages appearing in the console

* Message received: "message"
package springboot.amqp.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Service;

/**
 * Created by soumya on 7/2/17.
 */
@Service
public class MessageReceiver implements MessageListener{

    private static final Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

    @Override
    public void onMessage(Message message) {
        logger.debug("Message received: " + new String(message.getBody()));

    }

}
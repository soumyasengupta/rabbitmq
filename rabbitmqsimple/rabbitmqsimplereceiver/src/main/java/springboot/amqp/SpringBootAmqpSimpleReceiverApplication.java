package springboot.amqp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by soumya on 7/2/17.
 */
@SpringBootApplication
public class SpringBootAmqpSimpleReceiverApplication {

    public static void main(String[] args){

        SpringApplication.run(SpringBootAmqpSimpleReceiverApplication.class, args);

    }

}

Simple Sender And Receiver Application
======================================


Description
-----------

The application contains

* A sender application which sends messages to different queues
* A receiver application which receives messages from a queue. (If multiple queues are there, then we need to crop up multiple receiver instances)

Running The Applications
------------------------

Read the README.md files of individual applications





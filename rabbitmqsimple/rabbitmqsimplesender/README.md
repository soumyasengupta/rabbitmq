# The application is responsible for sending messages to queues
## Starting the application
* ensure RabbitMQ is running
* mvn spring-boot:run

## Queues
There are two queues named

* BLACK_QUEUE
* BLUE_QUEUE.

Messages can be sent to each of the two queues.

## Sending Messages

Need to call the following apis to send messages

* http://localhost:8080/amqp/send/black/{message}      *this will send messages to BLACK_QUEUE*
* http://localhost:8080/amqp/send/blue/{message} *this will send messages to BLUE_QUEUE*

package springboot.amqp.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by soumya on 7/2/17.
 */
@Configuration
public class RabbitMqConfiguration {

    @Autowired
    ConnectionFactory connectionFactory;

    final static String blackQueue = "BLACK_QUEUE";
    final static String blueQueue = "BLUE_QUEUE";

    @Bean(name = "blackQueue")
    public Queue queueBlack(){
        return new Queue(blackQueue,false);
    }

    @Bean(name = "blueQueue")
    public Queue queueBlue(){
        return new Queue(blueQueue, false);
    }

    @Bean
    public MessageConverter jsonMessageConverter()
    {
        return new Jackson2JsonMessageConverter();
    }

    @Bean(name="rabbitTemplate")
    public RabbitTemplate rabbitTemplate()
    {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }


}

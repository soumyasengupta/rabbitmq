package springboot.amqp.sender;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by soumya on 7/2/17.
 */
@Service
public class MessageSenderImpl implements MessageSender {

    @Autowired
    @Qualifier(value = "rabbitTemplate")
    RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(value = "blackQueue")
    Queue blackQueue;

    @Autowired
    @Qualifier(value = "blueQueue")
    Queue blueQueue;

    public String sendMessageToBlackQueue(String message){

        rabbitTemplate.convertAndSend(blackQueue.getName(), message);
        return "Sent message '"+message+"' to "+blackQueue.getName();

    }

    public String sendMessageToBlueQueue(String message){

        rabbitTemplate.convertAndSend(blueQueue.getName(),message);
        return "Sent message '"+message+"' to "+blueQueue.getName();

    }

}

package springboot.amqp.sender;

/**
 * Created by soumya on 7/2/17.
 */
public interface MessageSender {

    String sendMessageToBlackQueue(String message);

    String sendMessageToBlueQueue(String message);

}

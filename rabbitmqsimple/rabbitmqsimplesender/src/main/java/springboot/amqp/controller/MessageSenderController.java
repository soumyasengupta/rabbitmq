package springboot.amqp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.amqp.sender.MessageSender;

/**
 * Created by soumya on 7/2/17.
 */
@RestController
public class MessageSenderController {

    @Autowired
    MessageSender messageSender;

    @RequestMapping(value="/amqp/send/{queue}/{message}")
    public String sendMessageToBlackQueue(@PathVariable String queue, @PathVariable String message){

        if(queue.equalsIgnoreCase("black"))
            return messageSender.sendMessageToBlackQueue(message);
        else if(queue.equalsIgnoreCase("blue"))
            return messageSender.sendMessageToBlueQueue(message);
        else
            return "Invalid Queue (Should be one of black or blue) <br> e.g. /amqp/send/black/your_message <br> or <br> /amqp/send/blue/your_message";

    }

}

# This application is responsible for receiving messages from queues

## Description
* The sender application sends tasks to an exchange.
* The tasks are distributed among all queues listening to that exchange and received by the receivers listening to a queue who then execute the same
* The task is a simple POJO that represents a work to be executed by the receiver. It has a method named "execute" containing a "sleep(5000)" to impersonate a job of duration 5 seconds

## Start multiple receivers
* mvn spring-boot:run -Dserver.port=8081 -Dqueue.name=QUEUE_1 -Dexchange.name=FANOUT_EXCHANGE
* mvn spring-boot:run -Dserver.port=8082 -Dqueue.name=QUEUE_2 -Dexchange.name=FANOUT_EXCHANGE
* ...
* mvn spring-boot:run -Dserver.port=8090 -Dqueue.name=QUEUE_10 -Dexchange.name=FANOUT_EXCHANGE

## Testing the application
Now start sending messages to the queue

* http://localhost:8080/amqp/send/{n} *where "n" is the number of tasks to be sent to the queue*


You will find the receiving messages appearing in the console of all receivers

* Starting Received Task <Task{count=5, name='Name\_5', description='Description_5'}>
* Ending Received Task <Task{count=5, name='Name\_5', description='Description_5'}>



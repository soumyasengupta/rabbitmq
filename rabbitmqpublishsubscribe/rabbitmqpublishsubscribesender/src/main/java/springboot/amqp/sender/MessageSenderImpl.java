package springboot.amqp.sender;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import springboot.amqp.config.RabbitMqConfiguration;
import springboot.amqp.dto.Task;

/**
 * Created by soumya on 7/2/17.
 */
@Service
public class MessageSenderImpl implements MessageSender {

    @Value("${exchange.name}")
    public String EXCHANGE_NAME;

    @Autowired
    @Qualifier(value = "rabbitTemplate")
    RabbitTemplate rabbitTemplate;

    public Task sendMessageToExchange(Task task){

        rabbitTemplate.convertAndSend(EXCHANGE_NAME, task);
        return task;

    }

}

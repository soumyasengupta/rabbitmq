package springboot.amqp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springboot.amqp.dto.Task;
import springboot.amqp.sender.MessageSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by soumya on 7/2/17.
 */
@RestController
public class MessageSenderController {

    @Autowired
    MessageSender messageSender;

    @RequestMapping(value="/amqp/send/{noOfTimes}")
    public List<Task> sendMessageToBlackQueue(@PathVariable Integer noOfTimes){

            List<Task> taskList = new ArrayList<>();

            for(int count=1;count<=noOfTimes;count++) {
                Task task = new Task(count, "Name_"+count, "Description_"+count);
                task = messageSender.sendMessageToExchange(task);
                taskList.add(task);
            }

            return taskList;

    }

}

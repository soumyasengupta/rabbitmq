# The application is responsible for sending tasks to an exchange.
## Starting the application
* ensure RabbitMQ is running
* mvn spring-boot:run

## Sending Messages

Need to call the following APIs to send messages

* http://localhost:8080/amqp/send/{n} *where "n" is the number of tasks to be sent to the exchange*

The name of the exchange by default is 'FANOUT_EXCHANGE' but you can change the name to anything by either

* providing the name as command line arguement e.g. mvn spring-boot:run -Dexchange.name=NAME-OF-YOUR-CHOICE or 
* changing the name of exchange in application.properties

But if you are changing the name of exchange here, you also need to modify the exchange name in the message receiver application


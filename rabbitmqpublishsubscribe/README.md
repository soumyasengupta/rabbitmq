Application to implement a Publish Subscribe Model using Fanout Exchange
========================================================================


Description
-----------

The application contains

* A sender application which sends tasks to an exchange. The exchange then sends the messages to all the queues listening to that exchange
* 
* A receiver application which impersonates a consumer that listens to a queue to receive and executes tasks 

Task is nothing but a simple POJO containg a method that takes some time to execute, thus representing some kind work the consumer needs to perform on receiving the same.

Running The Applications
------------------------

Read the README.md files of individual applications





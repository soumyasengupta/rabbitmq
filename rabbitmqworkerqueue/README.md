Application to implement a Worker Queue 
=======================================


Description
-----------

The application contains

* A sender application which sends tasks to a single queue to be distributed in round-robin mode among multiple consumers listening to that queue
* A receiver application which impersonates a consumer that listens to the queue to receive and executes tasks 

Task is nothing but a simple POJO containg a method that takes some time to execute, thus representing some kind work the consumer needs to perform on receiving the same.

Running The Applications
------------------------

Read the README.md files of individual applications





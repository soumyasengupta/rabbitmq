# The application is responsible for sending tasks to a queue.
## Starting the application
* ensure RabbitMQ is running
* mvn spring-boot:run

## Queues
The queue is name named TASK_QUEUE


## Sending Messages

Need to call the following APIs to send messages

* http://localhost:8080/amqp/send/{n} *where "n" is the number of tasks to be sent to the queue*

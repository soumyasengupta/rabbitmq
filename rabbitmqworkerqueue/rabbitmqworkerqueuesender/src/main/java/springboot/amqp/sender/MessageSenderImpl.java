package springboot.amqp.sender;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import springboot.amqp.dto.Task;

/**
 * Created by soumya on 7/2/17.
 */
@Service
public class MessageSenderImpl implements MessageSender {

    @Autowired
    @Qualifier(value = "rabbitTemplate")
    RabbitTemplate rabbitTemplate;

    @Autowired
    @Qualifier(value = "taskQueue")
    Queue taskQueue;

    public Task sendMessageToTaskQueue(Task task){

        rabbitTemplate.convertAndSend(taskQueue.getName(), task);
        return task;

    }

}

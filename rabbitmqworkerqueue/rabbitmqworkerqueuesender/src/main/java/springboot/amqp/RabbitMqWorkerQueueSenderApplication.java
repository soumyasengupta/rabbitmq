package springboot.amqp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by soumya on 8/2/17.
 */
@SpringBootApplication
public class RabbitMqWorkerQueueSenderApplication {

    public static void main(String[] args){

        SpringApplication.run(RabbitMqWorkerQueueSenderApplication.class);

    }

}

# This application is responsible for receiving messages from queues

## Description
* The sender application sends tasks to a queue named "TASK_QUEUE".
* The tasks are distributed in round-robin order among the receivers who then execute the same
* The task is a simple POJO that represents a work to be executed by the receiver. It has a method named "execute" containing a "sleep(5000)" to impersonate a job of duration 5 seconds

## Start multiple receivers
* mvn spring-boot:run -Dserver.port=8081 
* mvn spring-boot:run -Dserver.port=8082 
* ...
* mvn spring-boot:run -Dserver.port=8090

## Testing the application
Now start sending messages to the queue

* http://localhost:8080/amqp/send/{n} *where "n" is the number of tasks to be sent to the queue*


You will find the receiving messages appearing in the console of all receivers

* Starting Received Task <Task{count=5, name='Name\_5', description='Description_5'}>
* Ending Received Task <Task{count=5, name='Name\_5', description='Description_5'}>



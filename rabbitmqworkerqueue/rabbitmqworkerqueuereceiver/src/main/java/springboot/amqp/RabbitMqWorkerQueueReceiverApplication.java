package springboot.amqp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by soumya on 7/2/17.
 */
@SpringBootApplication
public class RabbitMqWorkerQueueReceiverApplication {

    public static void main(String[] args){

        SpringApplication.run(RabbitMqWorkerQueueReceiverApplication.class, args);

    }

}

package springboot.amqp.receiver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import springboot.amqp.config.RabbitConfiguration;
import springboot.amqp.dto.Task;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by soumya on 7/2/17.
 */
@Component
public class MessageReceiver{

    private static final Logger logger = LoggerFactory.getLogger(MessageReceiver.class);

    @RabbitListener(queues = RabbitConfiguration.queueName)
    public void receiveTask(Task task) {
        logger.debug("Starting Received Task <" + task + ">");
        task.execute();
        logger.debug("Ending Received Task <" + task + ">");
    }

}